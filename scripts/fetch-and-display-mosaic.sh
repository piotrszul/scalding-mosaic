#!/bin/sh
set -e

#OUTPUT_DIR="target/output-$1-$2"
#OUTPUT_FILE="target/mosaic-$1-$2.jpg"

OUTPUT_DIR=$1
OUTPUT_FILE=$2

rm -rf $OUTPUT_DIR
hadoop fs -copyToLocal $OUTPUT_DIR target

./scripts/display-mosaic.sh $OUTPUT_DIR $OUTPUT_FILE