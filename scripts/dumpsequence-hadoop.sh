#!/bin/sh
set -e

hadoop jar target/scala-2.10/scalding-mosaic-assembly-1.0.jar  com.twitter.scalding.Tool  \
 -Dmapred.map.tasks=1 -Dmapred.reduce.tasks=5 -Dmapred.child.java.opts="-Xmx3096m" -Dmapred.max.split.size=20000000 \
 DumpSequence --hdfs --input data/img-set-small --output target/img-set-small-dump 


