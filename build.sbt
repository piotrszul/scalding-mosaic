import AssemblyKeys._ // put this at the top of the file

assemblySettings


name := "scalding-mosaic"

version := "1.0"

scalaVersion := "2.10.2"

// copied from scalding's build.sbt

resolvers += "Concurrent Maven Repo" at "http://conjars.org/repo"

libraryDependencies += "org.apache.hadoop" % "hadoop-core" % "0.20.2" % "provided"

libraryDependencies += "cascading" % "cascading-core" % "2.0.2"

libraryDependencies += "cascading" % "cascading-local" % "2.0.2"

libraryDependencies += "cascading" % "cascading-hadoop" % "2.0.2"

libraryDependencies += "cascading.kryo" % "cascading.kryo" % "0.4.4"

libraryDependencies += "com.twitter" % "meat-locker" % "0.3.0"

libraryDependencies += "com.twitter" % "maple" % "0.2.2"

libraryDependencies += "commons-lang" % "commons-lang" % "2.4"

// scalding (locally build)

libraryDependencies += "com.twitter" % "scalding-core_2.10" % "0.8.8"

libraryDependencies += "org.specs2" % "specs2_2.10" % "1.12.3"

javacOptions ++= Seq("-source", "1.6", "-target", "1.6")

// Invocation exception if we try to run the tests in parallel
parallelExecution in Test := false


// mainClass in assembly:= Some("com.twitter.scalding.Tool")

mergeStrategy in assembly <<= (mergeStrategy in assembly) {
      (old) => {
        case s if s.endsWith(".class") => MergeStrategy.last
        case s if s.endsWith("project.clj") => MergeStrategy.concat
        case s if s.endsWith(".html") => MergeStrategy.last
        case s if s.endsWith(".dtd") => MergeStrategy.last
        case s if s.endsWith(".xsd") => MergeStrategy.last
        case x => old(x)
      }
}


