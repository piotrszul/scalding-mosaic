#!/bin/sh
set -e

java -Xmx2048m -classpath target/scala-2.10/scalding-mosaic-assembly-1.0.jar:lib/img-filters-0.0.1-SNAPSHOT.jar:$HOME/.ivy2/cache/org.scala-lang/scala-library/jars/scala-library-2.10.2.jar:/usr/lib/hadoop/hadoop-common.jar:/usr/lib/hadoop/hadoop-auth.jar:/usr/lib/hadoop/lib/* \
name.pszul.mosaic.CreateMosaicLocal $1 $2 $3

