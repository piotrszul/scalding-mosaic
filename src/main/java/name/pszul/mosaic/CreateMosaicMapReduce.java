package name.pszul.mosaic;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import name.pszul.image.ImageUtils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.jobcontrol.ControlledJob;
import org.apache.hadoop.mapreduce.lib.jobcontrol.JobControl;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class CreateMosaicMapReduce {
    private static final String CONFIG_NAME_PSZUL_MOSAIC_SOURCE_FILE = "name.pszul.mosaic.sourceFile";
    private final static Path tempPath = new Path("target/mosaic-temp");

    public static class CreateMosiacMapper extends Mapper<Text, BytesWritable, IntWritable, Text> {
        private int cellSize = 30;
        private int computeSize = 10;
        private Map<Integer, BufferedImage> gridCells = null;

        private Map<Integer, BufferedImage> getGridCells(Context context) throws IOException {
            if (gridCells == null) {
                BufferedImage inputImage = ImageIO.read(FileSystem.get(context.getConfiguration()).open(
                        new Path(context.getConfiguration().get(CONFIG_NAME_PSZUL_MOSAIC_SOURCE_FILE))));
                int xCells = inputImage.getWidth() / cellSize;
                int yCells = inputImage.getHeight() / cellSize;
                HashMap<Integer, BufferedImage> result = new HashMap<Integer, BufferedImage>();
                for (int x = 0; x < xCells; x++) {
                    for (int y = 0; y < yCells; y++) {
                        result.put(x * 1000 + y, ImageUtils.scaleImage(ImageUtils.subimage(inputImage, x, y, cellSize),
                                computeSize, computeSize));
                    }
                }
                gridCells = result;
            }
            return gridCells;
        }

        public void map(Text key, BytesWritable value, Context context) throws IOException, InterruptedException {
            IntWritable position = new IntWritable();
            Text distanceAndId = new Text();
            int computeSize = 10;
            Map<Integer, BufferedImage> gridCells = getGridCells(context);
            try {
                BufferedImage libraryImage = ImageIO.read(new ByteArrayInputStream(value.getBytes(), 0, value
                        .getLength()));
                if (libraryImage.getRaster().getNumBands() == 3) {
                    BufferedImage scaledImage = ImageUtils.scaleImage(libraryImage, computeSize, computeSize);
                    for (Map.Entry<Integer, BufferedImage> gridCell : gridCells.entrySet()) {
                        double distance = ImageUtils.distanceRGB(gridCell.getValue(), scaledImage);
                        position.set(gridCell.getKey());
                        distanceAndId.set(String.format("%f %s", distance, key.toString()));
                        context.write(position, distanceAndId);
                    }
                }
            } catch (RuntimeException ex) {
            }
        }
    }

    public static class CreateMosaicCombiner extends Reducer<IntWritable, Text, IntWritable, Text> {
        public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException,
                InterruptedException {
            double minDistance = Double.MAX_VALUE;
            Text minDistanceAndId = null;
            for (Text value : values) {
                String[] distanceAndId = value.toString().split(" ");
                double distance = Double.parseDouble(distanceAndId[0]);
                if (minDistanceAndId == null || distance < minDistance) {
                    minDistanceAndId = new Text(value);
                    minDistance = distance;
                }
            }
            context.write(key, minDistanceAndId);
        }
    }

    public static class CreateMosaicReducer extends Reducer<IntWritable, Text, IntWritable, Text> {
        public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException,
                InterruptedException {
            double minDistance = Double.MAX_VALUE;
            String minId = null;
            for (Text value : values) {
                String[] distanceAndId = value.toString().split(" ");
                double distance = Double.parseDouble(distanceAndId[0]);
                if (minId == null || distance < minDistance) {
                    minId = distanceAndId[1];
                    minDistance = distance;
                }
            }
            context.write(key, new Text(minId));
        }
    }

    public static class JoinMapper extends Mapper<Text, BytesWritable, Text, BytesWritable> {
        private Map<String, List<Integer>> positions = null;

        private Map<String, List<Integer>> getPositions(Context context) throws IOException {
            if (positions == null) {
                positions = new HashMap<String, List<Integer>>();
                FileSystem fs = FileSystem.get(context.getConfiguration());
                for (FileStatus fStatus : fs.listStatus(tempPath)) {
                    if (!fStatus.getPath().getName().startsWith("_")) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(fs.open(fStatus.getPath())));
                        String line = in.readLine();
                        while (line != null) {
                            String[] posAndId = line.split("\t");
                            List<Integer> poss = positions.get(posAndId[1]);
                            if (poss == null) {
                                poss = new ArrayList<Integer>();
                                positions.put(posAndId[1], poss);
                            }
                            poss.add(Integer.parseInt(posAndId[0]));
                            line = in.readLine();
                        }
                        in.close();
                    }
                }
            }
            return positions;
        }

        public void map(Text key, BytesWritable value, Context context) throws IOException, InterruptedException {
            List<Integer> positions = getPositions(context).get(key.toString());
            if (positions != null) {
                for (Integer pos : positions) {
                    context.write(new Text(String.valueOf(pos)), value);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        FileSystem.get(conf).delete(tempPath, true);
        conf.set(CONFIG_NAME_PSZUL_MOSAIC_SOURCE_FILE, otherArgs[0]);
        Job createMosaicJob = new Job(conf, "CreateMosaic_CalculateDistanceStep");
        createMosaicJob.setJarByClass(CreateMosaicMapReduce.class);
        createMosaicJob.setMapperClass(CreateMosiacMapper.class);
        createMosaicJob.setCombinerClass(CreateMosaicCombiner.class);
        createMosaicJob.setReducerClass(CreateMosaicReducer.class);
        createMosaicJob.setOutputKeyClass(IntWritable.class);
        createMosaicJob.setOutputValueClass(Text.class);
        createMosaicJob.setInputFormatClass(SequenceFileInputFormat.class);
        FileInputFormat.addInputPath(createMosaicJob, new Path(otherArgs[1]));
        FileOutputFormat.setOutputPath(createMosaicJob, tempPath);
        Job joinJob = new Job(conf, "CreateMosaic_JoinStep");
        joinJob.setJarByClass(CreateMosaicMapReduce.class);
        joinJob.setMapperClass(JoinMapper.class);
        joinJob.setOutputKeyClass(Text.class);
        joinJob.setOutputValueClass(BytesWritable.class);
        joinJob.setInputFormatClass(SequenceFileInputFormat.class);
        joinJob.setOutputFormatClass(SequenceFileOutputFormat.class);
        joinJob.getConfiguration().setInt("mapred.reduce.tasks", 0);
        FileInputFormat.addInputPath(joinJob, new Path(otherArgs[1]));
        FileOutputFormat.setOutputPath(joinJob, new Path(otherArgs[2]));
        ControlledJob step1_createMosaic = new ControlledJob(createMosaicJob, Collections.<ControlledJob> emptyList());
        ControlledJob step2_createMosaic = new ControlledJob(joinJob,
                Arrays.asList(new ControlledJob[] { step1_createMosaic }));
        JobControl createMosaicFlow = new JobControl("CreateMosaic flow");
        createMosaicFlow.addJob(step1_createMosaic);
        createMosaicFlow.addJob(step2_createMosaic);
        Thread t = new Thread(createMosaicFlow);
        t.start();
        while (!createMosaicFlow.allFinished()) {
            Thread.sleep(1000);
        }
        System.exit(0);
    }
}