package name.pszul.hadoop

import org.apache.hadoop.io.SequenceFile

import org.apache.hadoop.io.Text
import org.apache.hadoop.io.BytesWritable
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import name.pszul.image.ImageUtils._
import name.pszul.hadoop.ImageConversions._

class ImageSequenceIterator(fileName: String, configuration:Configuration) extends Iterator[(String,Image)] {

    lazy val fs:FileSystem = FileSystem.get(configuration);
    lazy val path = new Path(fileName)
    lazy val paths = if (fs.isDirectory(path)) fs.listStatus(path).filter(f => !f.getPath().getName().startsWith("_")).map(f =>f.getPath()).toIterator else List(path).toIterator
    lazy val key: Text = new Text()
    lazy val value: BytesWritable = new BytesWritable()
    var reader:SequenceFile.Reader = null

    def hasNext:Boolean = {
    	while (reader == null || !reader.next(key, value)) {
    		if (paths.hasNext) {
    		  reader = new SequenceFile.Reader(fs, paths.next, configuration)    		  
    		} else {
    		  return false;
    		}
    	}
    	return true;
    }
    def next = (key.toString(), value.toImage)
}