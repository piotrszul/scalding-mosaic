package name.pszul.hadoop

import org.apache.hadoop.io.BytesWritable
import name.pszul.image.ImageUtils.Image
import name.pszul.image.ImageUtils.RawImage

class ExtendedBytesWritable(value: BytesWritable) {
  def toImage: Image = HadoopImageUtils.bytes2image(value)
  def toRawImage: RawImage = HadoopImageUtils.bytes2rawImage(value)
}

object ImageConversions {
  implicit def byteToExtendedBytes(bytes: BytesWritable): ExtendedBytesWritable = new ExtendedBytesWritable(bytes)
}