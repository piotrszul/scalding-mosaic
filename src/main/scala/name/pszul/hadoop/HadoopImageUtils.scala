package name.pszul.hadoop

import name.pszul.image.ImageUtils.Image
import org.apache.hadoop.io.BytesWritable
import javax.imageio.ImageIO
import java.io.ByteArrayInputStream
import name.pszul.image.ImageUtils.RawImage

object HadoopImageUtils {
  def bytes2image(value: BytesWritable) = ImageIO.read(new ByteArrayInputStream(value.getBytes(), 0, value.getLength()))

  def bytes2rawImage(value: BytesWritable): RawImage = value.getBytes().slice(0, value.getLength()).toList

}