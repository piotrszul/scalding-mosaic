package name.pszul.app

import org.apache.hadoop.io.BytesWritable
import org.apache.hadoop.io.Text
import com.twitter.scalding.Args
import com.twitter.scalding.FunctionImplicits.function2ToTupledFunction1
import com.twitter.scalding.Job
import com.twitter.scalding.WritableSequenceFile
import name.pszul.image.ImageUtils
import com.twitter.scalding.Tsv



class AnalyzeSequence(args: Args) extends Job(args) {

    //  proper scalding code
    val fields = ('key, 'value)
    val input = WritableSequenceFile[Text, BytesWritable](args("input"), fields)
    val output = Tsv(args("output"))

    val db = input
        .read
        .mapTo(('key, 'value) -> ('name, 'image)) { (key: Text, value: BytesWritable) => (key.toString(), value.getBytes().toList) }
        .filter('image) { image: List[Byte] => ImageUtils.isValidRGBImage(image) }
    	.mapTo(('name, 'image) -> ('name, 'size, 'widht, 'height)) {(name: String, imageSrc:List[Byte]) =>
    		val image = ImageUtils.imageFrom(imageSrc); 
    		(name, imageSrc.length, image.getWidth(), image.getHeight())
    	}
        .write(output)
}
