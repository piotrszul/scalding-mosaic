package name.pszul.app

import org.apache.hadoop.conf.Configuration
import name.pszul.image.ImageUtils._
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.io.File
import name.pszul.hadoop.ImageSequenceIterator

object ComposeMosaicApp {

    def main(args: Array[String]) {

        if (args.length != 2) {
            println("usage ComposeMosaicApp <input> <output>")
            sys.exit(1)
        }
        val input = args(0)
        val output = args(1)
        val tileSize = 50;
        val conf = new Configuration()
        val images = new ImageSequenceIterator(input, conf).map {
            case (key:String, image:Image) => ((key.toInt / 1000, key.toInt % 1000, image))
        }.toList;        
        val width = images.map(_._1).reduce(Math.max)
        val height = images.map(_._2).reduce(Math.max)
        
        println("Constructing mosaic - w: " + width + ", h: " + height + "tiles: " + images.length)
        
        val mosaicImage: Image = new Image(width * tileSize, height * tileSize, BufferedImage.TYPE_INT_RGB);
        images.foreach { case (x, y, image) => 
            mosaicImage.getGraphics().drawImage(scaleImage((image), tileSize, tileSize), x * tileSize, y * tileSize, null) }
        ImageIO.write(mosaicImage, "JPEG", new File(output))
    }
}