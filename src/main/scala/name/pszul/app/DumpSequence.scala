package name.pszul.app

import org.apache.hadoop.io.BytesWritable
import org.apache.hadoop.io.Text
import com.twitter.scalding.Args
import com.twitter.scalding.FunctionImplicits.function2ToTupledFunction1
import com.twitter.scalding.Job
import com.twitter.scalding.Tsv
import com.twitter.scalding.WritableSequenceFile



class DumpSequence(args: Args) extends Job(args) {

    //  proper scalding code
    val fields = ('key, 'value)
    val input = WritableSequenceFile[Text, BytesWritable](args("input"), fields)
    val output = Tsv(args("output"))

    val db = input
        .read
        .mapTo(('key, 'value) -> ('name, 'size)) { (key: Text, value: BytesWritable) => (key.toString(), value.getLength())}
        .write(output)
}
