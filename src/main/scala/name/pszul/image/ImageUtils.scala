package name.pszul.image
import java.awt.image.BufferedImage
import java.awt.image.DataBufferByte
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO
import com.jhlabs.image.AbstractBufferedImageOp
import com.jhlabs.image.GrayscaleFilter
import java.io.ByteArrayInputStream
import com.jhlabs.image.ScaleFilter
import java.awt.Color

/**
 * Set of Image processing utilities
 */

object ImageUtils {

    type Image = BufferedImage
    type RawImage = List[Byte]
    type ImageDistance = (Image, Image) => Double

    def distanceGrayscale(i1: Image, i2: Image): Double = {

        if (i1.getWidth() != i2.getWidth() || i1.getHeight() != i2.getHeight()) {
            throw new RuntimeException("Incompatible image sizes: " + i1.getWidth() + ", " + i2.getWidth() + ", " + i1.getHeight() + ", " + i2.getHeight());
        }

        var sumOfSquares = 0.0;
        for (i <- 0 until i1.getWidth()) {
            for (j <- 0 until i1.getHeight())
                sumOfSquares += (0.0 + i1.getRaster().getSample(i, j, 0) - i2.getRaster().getSample(i, j, 0)) * (0.0 + i1.getRaster().getSample(i, j, 0) - i2.getRaster().getSample(i, j, 0))
        }
        return math.sqrt(sumOfSquares)
    }

    def distanceRGB(i1: Image, i2: Image): Double = {

        if (i1.getWidth() != i2.getWidth() || i1.getHeight() != i2.getHeight()) {
            throw new RuntimeException("Incompatible image sizes: " + i1.getWidth() + ", " + i2.getWidth() + ", " + i1.getHeight() + ", " + i2.getHeight());
        }

        var sumOfSquares: Double = 0.0;
        for (i <- 0 until i1.getWidth()) {
            for (j <- 0 until i1.getHeight()) {
                var dist = 0.0
                for (b <- 0 until i1.getRaster().getNumBands()) {
                    val p1: Int = i1.getRaster().getSample(i, j, b);
                    val p2: Int = i2.getRaster().getSample(i, j, b);
                    dist += (p1 - p2) * (p1 - p2)
                }
                sumOfSquares += math.sqrt(dist)
            }
        }
        return sumOfSquares
    }

    def distanceHSB(weight: Seq[Float] = List(1.0f, 1.0f, 1.0f))(i1: Image, i2: Image): Double = {

        if (i1.getWidth() != i2.getWidth() || i1.getHeight() != i2.getHeight()) {
            throw new RuntimeException("Incompatible image sizes: " + i1.getWidth() + ", " + i2.getWidth() + ", " + i1.getHeight() + ", " + i2.getHeight());
        }

        var sumOfSquares: Double = 0;
        for (i <- 0 until i1.getWidth()) {
            for (j <- 0 until i1.getHeight()) {
                val px1 = for (b <- 0 until 3) yield i1.getRaster().getSample(i, j, b);
                val px2 = for (b <- 0 until 3) yield i2.getRaster().getSample(i, j, b);

                val hsv1 = new Array[Float](3)
                val hsv2 = new Array[Float](3)

                Color.RGBtoHSB(px1(0), px1(1), px1(2), hsv1)
                Color.RGBtoHSB(px2(0), px2(1), px2(2), hsv2)
                val dist: Seq[Float] = for (b <- 0 until 3) yield ((hsv1(b) - hsv2(b)) * (hsv1(b) - hsv2(b)) * weight(b))
                sumOfSquares += math.sqrt(dist.reduce(_ + _))
            }
        }
        return sumOfSquares
    }

    def avg(in: Image): Int = {
        val raster = in.getData()
        val samples: Array[Float] = raster.getSamples(0, 0, in.getWidth(), in.getHeight(), 0, Array.fill(in.getWidth() * in.getHeight())(0));
        return math.round(samples.reduce(_ + _) / (in.getWidth() * in.getHeight()));
    }

    def imageFrom(in: RawImage): Image = {
        return ImageIO.read(new ByteArrayInputStream(in.toArray))
    }

    def imageTo(in: Image): RawImage = {
        val outputBuffer = new ByteArrayOutputStream()
        ImageIO.write(in, "JPEG", outputBuffer)
        return outputBuffer.toByteArray().toList
    }

    def isValidRGBImage(image: RawImage): Boolean = {
        try {
            return imageFrom(image).getRaster().getNumBands() == 3
        } catch {
            case ex: RuntimeException => return false
        }
    }

    def max(f: RawImage => Double)(i1: RawImage, i2: RawImage): RawImage = {
        return if (f(i1) > f(i2)) i1 else i2
    }

    def apply(filter: AbstractBufferedImageOp)(src: Image): Image = {
        val dst = filter.createCompatibleDestImage(src, null)
        return filter.filter(src, dst)
    }

    def subimage(src: Image, x: Int, y: Int, size: Int): Image = {
        return src.getSubimage(x * size, y * size, size, size);
    }

    def scaleImage(src: Image, w: Int, h: Int): Image = {
        return apply(new ScaleFilter(w, h))(src).getSubimage(0, 0, w, h)
    }

    def unity(i1: Image, i2: Image): Double = {
        return (avg(i1) - avg(i2)) * (avg(i1) - avg(i2))
    }

    def toGrayScale(in: RawImage): RawImage = {
        return imageTo(apply(new GrayscaleFilter())(imageFrom(in)))
    }

}