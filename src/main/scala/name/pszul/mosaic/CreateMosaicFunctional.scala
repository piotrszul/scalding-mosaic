package name.pszul.mosaic

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.io.SequenceFile
import org.apache.hadoop.fs.Path
import name.pszul.image.ImageUtils._
import java.awt.image.BufferedImage
import org.apache.hadoop.io.Text
import org.apache.hadoop.io.BytesWritable
import javax.imageio.ImageIO
import java.io.File
import name.pszul.hadoop.ImageSequenceIterator


object CreateMosaicFunctional {
  
    type Pos = (Int,Int)
    type GridCell = (Pos, Image)
    type Grid = List[GridCell]
    
    def main(args: Array[String]) {
        
        if (args.length != 3) {
            println("usage CreateMosaicLocal <image> <input> <output>")
            sys.exit(1)
        }
        val cellSize = 30;
        val computeSize = 10;                	        
        val tileSize = 50;

        val pathToInputFile:String = args(0)
        val input = args(1)
        val output = args(2)
        
        val inputImage = ImageIO.read(new File(pathToInputFile))
        val xCells = inputImage.getWidth() / cellSize;
        val yCells = inputImage.getHeight() / cellSize;

        val sourceGrid = for (x <- (0 until xCells); y <- (0 until yCells))
                yield ((x,y), scaleImage(subimage(inputImage, x, y, cellSize), computeSize, computeSize));

        val libraryImages = new ImageSequenceIterator(input, new Configuration())
        
        val mosaicGrid = libraryImages
        	.flatMap{ case (id: String, img:Image) =>
            	        sourceGrid.map { case  (pos, subimage) => (pos, distanceRGB(img, subimage), img)}}.toList            	
            	.groupBy { case  (pos, distance, subimage) => pos }        
            	.map { case (p:Pos,l) =>  (p,l.reduce {(p1:(Pos,Double,Image),p2) => 
        	            if (p1._2 < p2._2) p1 else p2 }._3) }.toList
        	            
        val mosaic = mosaicGrid
        		.foldLeft(new Image(xCells * tileSize, yCells * tileSize, BufferedImage.TYPE_INT_RGB)) { case (mosaic:Image, (pos:Pos, img:Image)) => 
        		    mosaic.getGraphics().drawImage(scaleImage(img, tileSize, tileSize), pos._1 * tileSize, pos._2 * tileSize, null);
        		    mosaic
        }
        ImageIO.write(mosaic, "JPEG", new File(output))
    }
}
