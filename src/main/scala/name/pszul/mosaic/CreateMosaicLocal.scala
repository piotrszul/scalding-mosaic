package name.pszul.mosaic

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.io.SequenceFile
import org.apache.hadoop.fs.Path
import name.pszul.image.ImageUtils._
import java.awt.image.BufferedImage
import java.awt.Graphics
import org.apache.hadoop.io.Text
import org.apache.hadoop.io.BytesWritable
import javax.imageio.ImageIO
import java.io.File
import name.pszul.image.ImageUtils
import scala.Array.canBuildFrom

object CreateMosaicLocal {
  val conf = new Configuration()
  val fs = FileSystem.get(conf)

  def main(args: Array[String]) {

    if (args.length != 3) {
      println("usage CreateMosaicLocal <image> <input> <output>")
      sys.exit(1)
    }
    val cellSize = 30;
    val computeSize = 10;

    val pathToInputFile: String = args(0)
    val input = args(1)
    val output = args(2)

    val inputFile = new File(pathToInputFile)
    val inputImage = ImageIO.read(inputFile)
    val xCells = inputImage.getWidth() / cellSize;
    val yCells = inputImage.getHeight() / cellSize;

    val inputSetFile = new File(input)
    val inputs = if (inputSetFile.isDirectory()) inputSetFile.listFiles().filter { f: File => !f.getName().startsWith("_") } else Array(inputSetFile)

    var noOfImages: Long = 1;
    val startTime: Long = System.currentTimeMillis()

    val inputCellIndex = for (x <- (0 until xCells); y <- (0 until yCells))
      yield (x * yCells + y, scaleImage(subimage(inputImage, x, y, cellSize), computeSize, computeSize));

    val workingArray: Array[(Double, RawImage)] = new Array(xCells * yCells)

    val key = new Text()
    val value = new BytesWritable()

    inputs.foreach { f: File =>
      val reader = new SequenceFile.Reader(fs, new Path(f.getPath()), conf)
      while (reader.next(key, value)) {
        if ((noOfImages) % 1000 == 0) {
          val timeSoFar = System.currentTimeMillis() - startTime;
          val averageTimePerThousand = 1.0 * timeSoFar / noOfImages;
          println("Processed images: " + noOfImages + ", time: " + (timeSoFar / 1000) + " s, avgTime: " + averageTimePerThousand)
        }
        noOfImages += 1;

        val rawImage = value.getBytes().slice(0, value.getLength()).toList
        if (ImageUtils.isValidRGBImage(rawImage)) {
          val scaledInput: Image = scaleImage(imageFrom(rawImage), computeSize, computeSize)
          inputCellIndex.foreach {
            case (pos, image) =>
              val d = distanceRGB(image, scaledInput); if (workingArray(pos) == null || workingArray(pos)._1 > d) workingArray(pos) = (d, rawImage)
          }
        }
      }
    }

    val sequenceWriter = new SequenceFile.Writer(fs, conf, new Path(output), classOf[Text], classOf[BytesWritable]);
    workingArray.zipWithIndex.foreach {
      case ((d, image), i) => sequenceWriter.append(new Text(String.valueOf(i / yCells * 1000 + i % yCells)), new BytesWritable(image.toArray))
    }
    sequenceWriter.close()

    val timeSoFar = System.currentTimeMillis() - startTime;
    val averageTimePerThousand = 1.0 * timeSoFar / noOfImages;
    println("Total: processed images: " + noOfImages + ", time: " + (timeSoFar / 1000) + " s, avgTime: " + averageTimePerThousand)
  }
}
