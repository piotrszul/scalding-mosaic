package name.pszul.mosaic

import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.BytesWritable
import org.apache.hadoop.io.Text

import com.twitter.scalding.Args
import com.twitter.scalding.FunctionImplicits.function2ToTupledFunction1
import com.twitter.scalding.Job
import com.twitter.scalding.WritableSequenceFile

import cascading.tuple.Fields
import javax.imageio.ImageIO
import name.pszul.hadoop.ImageConversions.byteToExtendedBytes
import name.pszul.image.ImageUtils
import name.pszul.image.ImageUtils.Image
import name.pszul.image.ImageUtils.ImageDistance
import name.pszul.image.ImageUtils.RawImage
import name.pszul.image.ImageUtils.distanceRGB
import name.pszul.image.ImageUtils.imageFrom
import name.pszul.image.ImageUtils.scaleImage
import name.pszul.image.ImageUtils.subimage
import name.pszul.scalding.JobConfig

class SourceImage(val pathToImage: String, val cellSize: Integer, val computeSize: Integer) {

  lazy val inputImage = ImageIO.read(FileSystem.get(JobConfig.get).open(new Path(pathToImage)))
  lazy val xCells = inputImage.getWidth() / cellSize;
  lazy val yCells = inputImage.getHeight() / cellSize;
  lazy val inputCellIndex = for (x <- (0 until xCells); y <- (0 until yCells))
    yield (x * 1000 + y, scaleImage(subimage(inputImage, x, y, cellSize), computeSize, computeSize));

  def distanceToCells(distance: ImageDistance)(x: (String, RawImage)): Seq[(Int, Double, String)] = x match {
    case (imageId: String, rawImage: RawImage) => {
      val scaledInput: Image = scaleImage(imageFrom(rawImage), computeSize, computeSize)
      return inputCellIndex.map { case (pos, image) => (pos, distance(image, scaledInput), imageId) }
    }
  }
}

class CreateMosaicScalding(args: Args) extends Job(args) {
  val pathToImage = args("image")
  val cellSize = 30;
  val computeSize = 10;
  val fields = ('key, 'value)
  val input = WritableSequenceFile[Text, BytesWritable](args("input"), fields)
  val output = WritableSequenceFile[Text, BytesWritable](args("output"), fields)

  lazy val sourceImage = new SourceImage(pathToImage, cellSize, computeSize)

  val db = input.read
  val minPos = db
    .mapTo(('key, 'value) -> ('key, 'image)) { (key: Text, value: BytesWritable) => (key.toString(), value.toRawImage) }
    .filter('image) { image: List[Byte] => ImageUtils.isValidRGBImage(image) }
    .eachTo(Fields.ALL -> Fields.ALL) { f: Fields => JobConfig.load(f) }
    .flatMapTo(('key, 'image) -> ('pos, 'distance, 'key)) {
      (name: String, image: List[Byte]) => sourceImage.distanceToCells(distanceRGB)(name, image)
    }
    .groupBy('pos) { _.reduce(('distance, 'key) -> ('mindist, 'minkey)) { (x: (Double, String), y: (Double, String)) => if (x._1 < y._1) x else y } }
    .mapTo(('pos, 'minkey) -> ('pos, 'minkeyt)) { (pos: Int, minkey: String) => (pos, new Text(minkey)) }

  db.joinWithTiny('key -> 'minkeyt, minPos)
    .mapTo(('pos, 'value) -> ('key, 'value)) { (pos: Int, value: BytesWritable) => (new Text(pos.toString), value) }
    .write(output)
}

