package name.pszul.scalding;

import org.apache.hadoop.mapred.JobConf

import cascading.flow.FlowProcess
import cascading.flow.hadoop.HadoopFlowProcess
import cascading.operation.BaseOperation
import cascading.operation.FunctionCall
import cascading.tuple.Fields

object JobConfig {
    private class LoadAndSplitSourceImage(val f:Fields) extends BaseOperation[Any](f) with cascading.operation.Function[Any] {
	    def operate(flowProcess:FlowProcess[_],functionCall:FunctionCall[Any]):Unit = {
			JobConfig.initConfig(flowProcess);
			functionCall.getOutputCollector().add( functionCall.getArguments().getTuple())
		}   
    }

    private var _jobConf:JobConf = null
    
    private def initConfig(flowProcess:FlowProcess[_]):Unit = {
        if (_jobConf == null) {
        	_jobConf = flowProcess  match  {
		    	case hfp:HadoopFlowProcess => hfp.getJobConf()
		    	case _ => error("Not a hadoop Flow Process")
        	}
        	println("Hadoop job config loaded:" + _jobConf)
        }
    }    

    def get:JobConf = {
        if (_jobConf == null) {
            error("JobConf not initialized. Please use eachTo{load}" )
        }
        return _jobConf;
    }
    
    def load(f:Fields):cascading.operation.Function[Any] = {
        return new LoadAndSplitSourceImage(f);
    }
    
}