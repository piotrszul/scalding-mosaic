#!/bin/sh
set -e
./run-createmosaic-hadoop.sh data/face.jpg data/test-img-set target/mosaic-test-hadoop
./scripts/fetch-and-display-mosaic.sh target/mosaic-test-hadoop target/mosaic-test-hadoop.jpg
