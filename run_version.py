#!/usr/bin/python
from time import time
import os
from sys import  argv

if (len(argv) < 2):
	print("Usage run_version <version> <dataset>")
        exit(1)

times = 3 if (len(argv) < 3) else int(argv[3])
command = "./run-createmosaic-%s.sh data/face.jpg data/img-set-%s target/output-%s-%s" % (argv[1], argv[2], argv[1], argv[2])
print("Running times: %s, command: %s" % (times, command))

run_times = []

for i in range(0,times):
	startTime = time()
	os.system(command)
	endTime  = time()
        run_time = (endTime-startTime)
        run_times.append(run_time)
	print("This run time: %s" % run_time)

print("Summary")
print("Average run time from %s runs: %ss" % (times, sum(run_times)/times))
