#!/bin/bash
set -e

hadoop jar target/scala-2.10/scalding-mosaic-assembly-1.0.jar com.twitter.scalding.Tool \
 -Dmapred.reduce.tasks=5 -Dmapred.child.java.opts="-Xmx3096m" -Dmapred.max.split.size=45000000 -Dmapred.min.split.size=40000000 \
 name.pszul.mosaic.CreateMosaicScalding --hdfs --input $2 --image $1 --output $3

