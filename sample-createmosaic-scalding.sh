#!/bin/bash

set -e
./run-createmosaic-scalding.sh data/face.jpg data/test-img-set target/mosaic-test-scalding
./scripts/fetch-and-display-mosaic.sh target/mosaic-test-scalding target/mosaic-test-scalding.jpg
