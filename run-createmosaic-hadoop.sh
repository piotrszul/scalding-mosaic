#!/bin/sh
set -e
hadoop fs -rm -r -f -skipTrash $3
hadoop jar target/scala-2.10/scalding-mosaic-assembly-1.0.jar  name.pszul.mosaic.CreateMosaicMapReduce  \
 -Dmapred.reduce.tasks=5 -Dmapred.child.java.opts="-Xmx3024m" \
 $1 $2 $3


